extern crate dbus;
extern crate serde;

#[macro_use]
extern crate log;
extern crate stderrlog;

mod commands;
mod docker_containers;
mod mute_pulseaudio;
mod resume_listener;
mod screens_off;
mod sleep_config;
mod systemd_units;

pub fn run(
    reboot_menu_entry: Option<&String>,
    docker_containers: Vec<&String>,
    systemd_units: Vec<&String>,
    turn_screens_off: bool,
    mute_pulseaudio: bool,
) -> Result<(), String> {
    // Check if a reboot is allowed before doing any modifications.
    debug!("Checking if reboot is allowed");
    if !commands::is_reboot_allowed()? {
        return Err("reboot not allowed by systemctl".to_owned());
    }

    // Keep optionally changed configuration until end of execution.
    let mut _config_file_adjustment = None;

    // Reboot if desired.
    if let Some(entry) = reboot_menu_entry {
        info!("Preparing reboot");
        // Change configuration to reboot after hibernate.
        _config_file_adjustment = Some(sleep_config::SleepConfigAdjustment::new()?);

        // Set menu entry to reboot to.
        commands::set_reboot_entry(entry)?;
    }

    // Prepare listening for resume signals.
    let resume_awaiter = resume_listener::Listener::new()?;

    let mut docker_container_stoppers = Vec::new();

    // Stop docker containers
    for container in docker_containers {
        docker_container_stoppers.push(docker_containers::DockerContainerStopper::new(container)?);
    }

    let mut systemd_unit_stoppers = Vec::new();

    // Stop systemd units
    for unit in systemd_units {
        systemd_unit_stoppers.push(systemd_units::SystemdUnitStopper::new(unit)?);
    }

    let _screens_off = if turn_screens_off {
        Some(screens_off::ScreensOff::new()?)
    } else {
        None
    };

    let _mute_pulseaudio = if mute_pulseaudio {
        Some(mute_pulseaudio::MutePulseaudio::new())
    } else {
        None
    };

    // Start hibernating.
    debug!("Triggering hibernation");
    commands::trigger_hibernate()?;

    // Wait for the resume signal.
    resume_awaiter.wait();

    // Destroying docker_container_stoppers will start all stopped containers now.
    // Destroying _config_file_adjustment will reset the config file now.
    // Destroying _mute_pulseaudio will unmute sinks now.

    Ok(())
}
