use dbus::blocking::Connection;
use dbus::{arg, Message};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;

pub struct Listener {
    done: Arc<AtomicBool>,
    dbus_connection: Connection,
}

struct PrepareForSleep {
    active: bool,
}

impl arg::ReadAll for PrepareForSleep {
    fn read(i: &mut arg::Iter) -> Result<Self, arg::TypeMismatchError> {
        Ok(PrepareForSleep { active: i.read()? })
    }
}

impl Listener {
    /// Start listening for resume events.
    pub fn new() -> Result<Listener, String> {
        let instance = Listener {
            done: Default::default(),
            dbus_connection: match Connection::new_system() {
                Ok(c) => c,
                Err(err) => {
                    return Err(format!("Failed connecting to DBus: {}", err));
                }
            },
        };
        let done = instance.done.clone();
        instance
            .dbus_connection
            .add_match(
                dbus::message::MatchRule::new_signal(
                    "org.freedesktop.login1.Manager",
                    "PrepareForSleep",
                ),
                move |m: PrepareForSleep, _: &Connection, _: &Message| {
                    debug!(
                        "Received org.freedesktop.login1.Manager with active = {}",
                        m.active
                    );
                    if !m.active {
                        done.store(true, Ordering::Release);
                    }
                    true
                },
            )
            .expect("failed to add match to dbus connection");

        Ok(instance)
    }

    /// Wait for the resume event.
    pub fn wait(&self) {
        while !self.done.load(Ordering::Acquire) {
            match &self.dbus_connection.process(Duration::new(1000, 0)) {
                Ok(_) => {}
                Err(e) => {
                    error!("Processing DBus messages failed: {}", e);
                }
            }
        }
    }
}
